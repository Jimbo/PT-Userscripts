// ==UserScript==
// @name         BTN Add Format
// @version      0.7
// @downloadURL  https://gitea.com/Jimbo/PT-Userscripts/raw/branch/main/btn-addformat.user.js
// @updateURL    https://gitea.com/Jimbo/PT-Userscripts/raw/branch/main/btn-addformat.user.js
// @description  Adds a button for each torrent category
// @author       JimboDev
// @match        https://broadcasthe.net/upload.php
// @match        https://broadcasthe.net/series.php?id=*
// @icon         https://broadcasthe.net/favicon.ico
// @grant        GM.setValue
// @grant        GM.getValue
// @grant        GM.deleteValue
// @grant        GM_openInTab
// @grant        GM_xmlhttpRequest
// @run-at       document-end
// ==/UserScript==

function isVisible(e) {
    return !!( e.offsetWidth || e.offsetHeight || e.getClientRects().length );
}

async function handle_upload_page() {
    var id = window.location.hash.substring(1);
    const formats = await GM.getValue(id);
    GM.deleteValue(id)
    if (!formats) return;

    var type = document.querySelector('select#categories');
    if (!isVisible(type)) return;
    var data = JSON.parse(formats);

    if (data.submit) {
        var format = document.querySelector('input#title');
        format.value = data.format;
        return
    } else {
        const upload_form = document.querySelector("form#upload_table")
        upload_form?.setAttribute("action", upload_form.getAttribute("action") + "#" + id)
    }

    switch(data.type) {
        case "episode":
            type.selectedIndex = 0
            break;
        case "season":
            type.selectedIndex = 1
            break;
        default:
            return;
    }
    try {
        type.onchange();
    } catch {}

    var releasename = document.querySelector('select#scene_yesno');
    releasename.selectedIndex = 2;
    $("select#scene_yesno").change()

    var series = document.querySelector('input#auto_series');
    series.value = data.title;

    var format = document.querySelector(data.type === 'episode' ? 'input#auto_title' : 'input#auto_season');
    format.value = data.format;

    data.submit = true
    GM.setValue(id, JSON.stringify(data));
    $('input[name="tvdb"]').click()
}

function handle_button_click(group, id, event) {
    var title = group.querySelector('strong a');
    var data = {
        "title": document.querySelector('div#content div div div[class="head"] strong')?.textContent,
        "format": title.text.trim().replaceAll('\n',' - '),
        "type": title.className,
        "submit": false
    }
    GM.setValue(id, JSON.stringify(data));

    GM_openInTab(event.target.href);
}


function handle_torrent_page() {

    const torrents = document.querySelectorAll('div#content tr.group_torrent.discog.first');

    torrents.forEach((el,key) => {
        var group = el.querySelector('td[class*="group discog"]');


        let div = document.createElement('div');
        div.style.cssText = "position:relative;"

        let strong = document.createElement('strong');

        let link = document.createElement('a');
        link.id = `add_format_${key}`;
        link.class = group?.querySelector('a')?.className
        link.href = "upload.php#auto_addformat_"+key;
        link.onclick = function(event) { handle_button_click(group, "auto_addformat_"+key, event); return false; };
        link.text = "Add Format";
        // link.style.cssText = "bottom:0;position:absolute;height:15px;width:100%;"

        // var title = group.querySelector('strong a');

        // group.style.cssText = group.style.cssText+"position:relative;";
        // var height = Math.max(50, title.text.trim().split("\n").length * 25);
        // if (height < 50)
        //     group.height = group.height + height;
        strong?.appendChild(link);
        div?.appendChild(strong);
        group?.appendChild(div);
    })

    handle_missing_items()

}

function fetchJSON(url, method = "GET", timeout = 30000) {
    return new Promise((resolve, reject) => {
        GM_xmlhttpRequest({
            headers: {
                "Accept": "application/json"
            },
            method: method,
            url: url,
            timeout,
            ontimeout: function() {
                reject(new Error(`Request timed out after ${timeout}ms`));
            },
            onerror: function(err) {
                reject(err ? err : new Error('Failed to fetch'))
            },
            onload: function(response) {
                resolve(JSON.parse(response.responseText));
            }
        })
    });
}

function fetchHTML(url, method = "GET", timeout = 30000) {
    return new Promise((resolve, reject) => {
        GM_xmlhttpRequest({
            headers: {
                "Accept": "text/html"
            },
            method: method,
            url: url,
            timeout,
            ontimeout: function() {
                reject(new Error(`Request timed out after ${timeout}ms`));
            },
            onerror: function(err) {
                reject(err ? err : new Error('Failed to fetch'))
            },
            onload: function(response) {
                const parser = new DOMParser();
                const result = parser.parseFromString(response.responseText, "text/html").body;
                resolve(result);
            }
        })
    });
}

async function handle_missing_items() {
    const poster = document.querySelector('#content > div.thin > div.sidebar > div:nth-child(1)')
    const imdb = document.querySelector("#content > div.thin > div.sidebar > div:nth-child(2) > ul > table > tbody > tr:nth-child(12) > td:nth-child(2) > a:nth-child(2)")
    const tvdb = document.querySelector("#content > div.thin > div.sidebar > div:nth-child(2) > ul > table > tbody > tr:nth-child(12) > td:nth-child(2) > a:nth-child(3)")
 
    const tvdb_doc = await fetchHTML(tvdb.href)
    
    const tvdb_id = tvdb_doc.querySelector('#save_add_season > input[name="series"]')

    if (!tvdb_id) return

    const tvdb_res = await fetchJSON(`https://skyhook.sonarr.tv/v1/tvdb/shows/en/${tvdb_id.value}`)

    poster?.insertAdjacentHTML("afterend", `<div class="box"> <div class="head"><strong>Add Missing Format (${tvdb_res["title"]})</strong></div> <div style="padding: 0px 10px 10px 10px"> <select name="Add Format" id="addFormat-list"> <option value="">--</option> </select> <button style="float: right" id="addFormat-btn">Add format</button> </div></div>`)
    const torrents = document.querySelectorAll('div#content tr.group_torrent td.group a[title]');

    let options = {}

    for (const torrent of torrents) {
        options[torrent.textContent?.trim()] = true
    }

    const first = document.querySelector("div#content tr.group_torrent td.group a.episode")
    const isDate = first?.textContent?.match(/\d{4}\.\d{2}\.\d{2}/)

    for (const season of tvdb_res["seasons"].reverse()) {
        if (!options[`Season ${season["seasonNumber"]}`]) {
            add_mission_option(`Season ${season["seasonNumber"]}`)
        }
    }

    for (const episode of tvdb_res["episodes"].reverse()) {
        if (options[`Season ${episode["seasonNumber"]}`]) continue


        const date = (episode['airDate'] || "UNKNOWN").replaceAll("-", ".")

        const season = String(episode["seasonNumber"]).padStart(2, '0')
        const ep = String(episode["episodeNumber"]).padStart(2, '0')
        const elm = `S${season}E${ep}`



        if (!options[elm] && !options[date]) {
            add_mission_option(isDate ? date : elm)
        }
    }


    const btn = document.querySelector('button#addFormat-btn')

    btn.onclick = function() {
        const select = document.querySelector('select#addFormat-list')

        const selected = select.selectedOptions[0].value

        if (!selected) return

        const id = `addformat_${selected.replace(" ", "")}`

        var data = {
            "title": document.querySelector('div#content div div div[class="head"] strong')?.textContent,
            "format": selected,
            "type": selected.includes("Season") ? "season" : "episode" ,
            "submit": false
        }
        GM.setValue(id, JSON.stringify(data));

        GM_openInTab(`https://broadcasthe.net/upload.php#${id}`);

        return false;
    }

}

function add_mission_option(option) {
    const select = document.querySelector('select#addFormat-list')

    var opt = document.createElement('option');
    opt.value = option;
    opt.innerHTML = option;
    select.appendChild(opt);
}


(function() {
    'use strict';
    if (window.location.pathname === '/upload.php') {
        if (window.location.hash)
            handle_upload_page();
    } else if (window.location.pathname === "/series.php") {
        handle_torrent_page();
    }
})();