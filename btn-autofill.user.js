// ==UserScript==
// @name         BTN Upload Autofill
// @version      0.9.7
// @downloadURL  https://gitea.com/Jimbo/PT-Userscripts/raw/branch/main/btn-autofill.user.js
// @updateURL    https://gitea.com/Jimbo/PT-Userscripts/raw/branch/main/btn-autofill.user.js
// @description  Autofills upload info based on mediainfo
// @author       JimboDev
// @match        https://broadcasthe.net/upload.php
// @icon         https://broadcasthe.net/favicon.ico
// @grant        none
// @run-at       document-end
// ==/UserScript==

const hasFastTorrent = true; // True will tick FastTorrent, False will not do anything.
const formatReleaseName = true; // True will automatically handle the Release Name, False will not do anything.
const defaultFormat = "MKV"; // Use any can also be empty: AVI,MKV,VOB,MPEG,MP4,ISO,WMV,TS,M4V,M2TS,Mixed
const defaultSource = "WEB-DL"; // Use any can also be empty: HDTV,PDTV,DSR,DVDRip,TVRip,VHSRip,Bluray,BDRip,BRRip,DVD5,DVD9,HDDVD,WEB-DL,WEBRip,BD5,BD9,BD25,BD50,Mixed,Unknown
const defaultCodex = "H.264"; // Use any can also be empty: XViD,MPEG2,DiVX,DVDR,VC-1,H.264,H.265,WMV,BD,x264-Hi10P,VP9,Mixed
const defaultRes = "1080p"; // Use any can also be empty: SD,720p,1080p,1080i,2160p,Portable Device,Mixed
const defaultOrigin = "P2P"; // Use any can also be empty: None,Scene,P2P,User,Mixed

//These formats can be extended, let me know if any are missing as I can add more.
const releaseSources = /(?<!DTS[ .-]?HD[ .-]?)(NF|NETFLIX|Netflix|CR|CRUNCYROLL|ITUNES|IT|ATVP|DSNP|AMZN|AMAZON|FUNI|FUNIMATION|WAKANIM|ANIMELAB|HIDIVE|HIDI|VRV|HULU|PCOK|HMAX|BCORE|MA|IP|iP|iT|CBS|PMTP|MAX|CRAV|UKTV|KKTV|F1TV)\b/; 

const audioFormat_map = new Map();
audioFormat_map.set("E-AC-3 JOC", "DDPA");
audioFormat_map.set("E-AC-3", "DDP");
audioFormat_map.set("AC-3", "DD");
audioFormat_map.set("AAC LC SBR", "AAC");
audioFormat_map.set("AAC LC", "AAC");
audioFormat_map.set("DTS XLL", "DTS-HD.MA");
audioFormat_map.set("DTS XBR", "DTS-HD.HR");
audioFormat_map.set("MLP FBA", "TrueHD");
audioFormat_map.set("MLP FBA 16-ch", "TrueHD");

//////////////////////////////////////////////////////////
//DO NOT MODIFY BELOW UNLESS YOU KNOW WHAT YOU ARE DOING//
//////////////////////////////////////////////////////////

const langs = {
    Albanian: "Albania",
    Arabic: "Arableague",
    Bengali: "Bangladesh",
    Bulgarian: "Bulgaria",
    Catalan: "Spain",
    Czech: "Czechrep",
    Chinese: "China",
    Croatian: "Croatia",
    Danish: "Denmark",
    Dutch: "Netherlands",
    English: "England",
    Estonian: "Estonia",
    fil: "Philippines",
    Finnish: "Finland",
    French: "France",
    Galician: "Spain",
    German: "Germany",
    Greek: "Greece",
    Hebrew: "Israel",
    Hindi: "India",
    Hungarian: "Hungary",
    Icelandic: "Iceland",
    Indonesian: "Indonesia",
    Italian: "Italy",
    Japanese: "Japan",
    Kannada: "India",
    Korean: "South Korea",
    Latvian: "Latvia",
    Lithuanian: "Lithuania",
    Malay: "Malaysia",
    Malayalam: "India",
    Norwegian: "Norway",
    "Norwegian Bokmal": "Norway",
    Persian: "Iran",
    Polish: "Poland",
    Portuguese: "Portugal",
    Romanian: "Romania",
    Russian: "Russia",
    Serbian: "Serbia",
    Slovak: "Slovakia",
    Slovenian: "Slovenia",
    Spanish: "Spain",
    Swedish: "Sweden",
    Tamil: "Srilanka",
    Telugu: "India",
    Thai: "Thailand",
    Turkish: "Turkey",
    Ukrainian: "Ukraine",
    Vietnamese: "Vietnam",
    Welsh: "Wales",
};

const codec_map = new Map();
codec_map.set("HEVC", "H.265")
codec_map.set("AVC", "H.264")

function parse_mediainfo(mediainfo) {
    var mediainfo_sections = Array.from(mediainfo.matchAll(/^(General|Video|Audio)( #1)?\n(((.+)\n)+)/mg));

    if (mediainfo.length === 0)
        return;

    var output = {};

    mediainfo_sections.forEach(mediainfo_section => {
        var values = {};

        var mediainfo_values = Array.from(mediainfo_section[3].matchAll(/^([\(\)/,#\w\d *\.]+):\s(.+)$/mg));

        mediainfo_values.forEach( value => {
            values[value[1].trim()] = value[2];
        })

        output[mediainfo_section[1].trim()] = values;
    });

    return output;
}

function source_check(mediainfo, release_name) {
    var file_name = mediainfo["General"]["Complete name"];
    var file_name_l = file_name.toLowerCase();

    if (file_name_l.match(/\bweb\b/g) || file_name_l.includes("web-dl"))
        return "WEB-DL";
    else if (file_name_l.includes("webrip"))
        return "WEBRip";
    else if (file_name_l.includes("blu-ray") || file_name_l.includes("bluray") || file_name.match(/\bBD\b/g))
        return release_name ? "BluRay" : "Bluray";
    else if (file_name_l.includes("hdtv"))
        return "HDTV"

    return ""

}

function getFullNumber(num) {
    return num.match(/\d/g).join("");
}

const RES_TO_CHECK = Object.entries({
    3840: "2160",
    1920: "1080",
    1280: "720",
});
function resolution_check(mediainfo) {
    var height = getFullNumber(mediainfo["Video"]["Height"].toLowerCase());
    var width = parseInt(getFullNumber(mediainfo["Video"]["Width"].toLowerCase()));

    var scan_type = "p";

    if (mediainfo["Video"]["Scan type"])
        scan_type = mediainfo["Video"]["Scan type"].match(/(Interlaced|MBAFF)/) ? "i" : "p";

    for (const [i ,[res, out]] of RES_TO_CHECK.entries()) {
        let percent = parseInt(res)/width

        if (0.95<=percent && percent<= 1.05) {
            height = out
            break;
        }
    }
    
    if (parseInt(height) < 680) {
        return "SD";
    }
    return height + scan_type;
}

function handle_dropdownFill(element, value) {
    Array.apply(null,element.options).every(option => {
        if (option.text === value) {
            element.selectedIndex = option.index
            return false;
        }
        return true;
    })
}

function handle_languagecountry(mediainfo) {

    if (!("Language" in mediainfo["Audio"])) return;

    var audio_lang = mediainfo["Audio"]["Language"].match(/(\w+(\s\w+)*)/)[0];

    var mapped_lang = langs[audio_lang];

    if (!mapped_lang || mapped_lang === "England") return;

    var foreign = document.querySelector('input#international_box');
    foreign.checked = true;

    var country = document.querySelector('select#country');
    handle_dropdownFill(country, mapped_lang);



}

function handle_audioinfo(mediainfo) {

    var audio_format_info = mediainfo["Audio"]["Format"];
    var audio_channel_info = mediainfo["Audio"]["Channel(s)"];
    var audio_channel_type_info = mediainfo["Audio"]["Channel layout"] || mediainfo["Audio"]["Channel positions"] || "";
    var audio_format = audioFormat_map.get(audio_format_info)|| audio_format_info;

    var audio_com = mediainfo["Audio"]["Commercial name"] || "";
    if (audio_format === "TrueHD" && audio_com && audio_com.includes('Dolby Atmos')) {
        audio_format += "A";
    }

    var channels = audio_channel_info.match(/^\d+/)[0];

    if (audio_channel_type_info.match(/LFE/)){
        channels = `${parseInt(channels) - 1}.1`;
    } else {
        channels += ".0";
    }

    return audio_format + channels;


}

const release_group_res = [
    /(?<!Blu|WEB|DTS)-\s?([\w\.]+)\)?\.\w+$/,
    /^\[([\w\s]+)\]/,
]

function handle_releasegroup(filename) {

    for (var i = 0; i < release_group_res.length; i++) {
        var release_group_re = filename.match(release_group_res[i]);
        if (release_group_re){
            return release_group_re[1];
        }
    } 
    return "UNKNOWN_RELEASE_GROUP";
}

function handle_releasesource(filename) {
    var source_match = filename.match(releaseSources);    
    if (!source_match) {
        return "";
    }

    if (source_match === "Netflix" || source_match === "NETFLIX") {
        return ".NF";
    } else if (source_match === "CRUNCYROLL") {
        return ".CR";
    } else if (source_match === "ITUNES" || source_match === "IT") {
        return ".iT";
    } else if (source_match === "FUNIMATION") {
        return ".FUNI";
    }
    return "."+source_match[1];
}

function strip_restricted_chars(text) {
    return text.replaceAll(/\./g,"").replaceAll("&","and").split(" ").filter(word => !word.match(/^[^abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789]+$/)).join(".").replaceAll(/[^abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789\.]+/g,'');
}

function handle_releasename(mediainfo) {
    let upload_table = document.querySelector('div#dynamic_form > table#table_manual_upload');
    let type = document.querySelector('select#categories');

    let release_name_input = upload_table.querySelector("input#scenename");
    if (release_name_input.value)
        return
    
    let file_name = mediainfo["General"]["Complete name"].split('\\').pop().split('/').pop();

    // Episodes for anime follow filename format
    let genre = document.querySelector('input#tags');
    if (genre.value.includes("Anime") && type.value == "Episode") {
        var file_name_wo_ext = file_name.split(".").slice(0,-1).join(".");

        release_name_input.value = file_name_wo_ext;

        return;
    }

    let release_name = "";
    
    let tv_name = upload_table.querySelector("input#artist");

    release_name += strip_restricted_chars(tv_name.value);


    let tv_title_input = upload_table.querySelector("input#title");
    let tv_title = tv_title_input.value;
    if (type.value === "Season") {
        let seasonNum = tv_title.match(/Season (\d+)/)[1];
        if (seasonNum.length === 1)
            seasonNum = "0"+seasonNum;
        tv_title = "S"+seasonNum;
    } else if (type.value === "Episode") {
        tv_title = strip_restricted_chars(tv_title)
        let seasonNum = tv_title.match(/Season\.(\d+)/);
        if (seasonNum) {
            seasonNum = seasonNum[1]
            if (seasonNum.length === 1) {
                seasonNum = "0"+seasonNum;
            }
            tv_title = `S${seasonNum}` + tv_title.replaceAll(/Season\.(\d+)/g, '')
        }
    }

    release_name += "."+tv_title;

    let resolution = resolution_check(mediainfo);
    let source = source_check(mediainfo, true) || defaultSource || "SOURCE_UNKNOWN";
    let file_video_codec = codec_map.get( mediainfo["Video"]["Format"]) || mediainfo["Video"]["Format"];
    if (file_video_codec === "H.264") {
        if (mediainfo["Video"]["Bit depth"] === "10 bits") {
            file_video_codec = "x264-Hi10P";
            if (source === "WEB-DL") {
                source = "WEBRip";
            }
        }
    }

    if (source === 'BluRay') {
        if (!mediainfo['Video']['Encoding settings'] || file_name.match(/\bremux\b/i)) {
            source += '.Remux';
        } else if (file_video_codec === "H.264") {
            file_video_codec = "x264";
        } else if (file_video_codec === "H.265") {
            file_video_codec = "x265";
        }

    }

    
    let hdr_format = mediainfo["Video"]["HDR format"] || "";

    if (hdr_format.includes("Dolby Vision")) {
        release_name += ".DV";
    }
    if (hdr_format.includes("HDR10")) {
        release_name += ".HDR";
    }

    if (resolution!=="SD")
        release_name += "."+resolution;

    release_name += handle_releasesource(file_name)

    release_name += "."+source;

    release_name += "."+handle_audioinfo(mediainfo);

    release_name += "."+file_video_codec;

    release_name += "-"+handle_releasegroup(file_name).replaceAll(/\s/g,'');

    release_name_input.value = release_name;
}


function handle_autofill(mediainfo) {

    var upload_table = document.querySelector('div#dynamic_form > table#table_manual_upload_2');
    if (!upload_table || !mediainfo || Object.keys(mediainfo).length === 0)
        return;

    // Handle Container selector
    var file_name = mediainfo["General"]["Complete name"];
    var file_ext = file_name.split(".").pop().toUpperCase()

    var format_select = upload_table.querySelector("select#format");
    handle_dropdownFill(format_select, file_ext);

    var source = source_check(mediainfo) || defaultSource;

    // Handle Codec
    var file_video_codec = codec_map.get( mediainfo["Video"]["Format"]) || mediainfo["Video"]["Format"];
    if (file_video_codec === "H.264" && mediainfo["Video"]["Bit depth"] === "10 bits") {
        file_video_codec = "x264-Hi10P";
        if (source === "WEB-DL") {
            source = "WEBRip";
        }
    }

    var codec_select = upload_table.querySelector("select#bitrate");
    handle_dropdownFill(codec_select, file_video_codec);

    // Handle Source
    if (source) {
        var media_select = upload_table.querySelector("select#media");
        handle_dropdownFill(media_select, source);
    }

    // Handle Resolution
    var resolution = resolution_check(mediainfo);

    var resolution_select = upload_table.querySelector("select#resolution");
    handle_dropdownFill(resolution_select, resolution);


    if (formatReleaseName) handle_releasename(mediainfo);
    handle_languagecountry(mediainfo);

}

(function() {
    'use strict';

    const upload_table = document.querySelector('div#dynamic_form > table#table_manual_upload_2');
    const upload_table_alt = document.querySelector('div#dynamic_form > table#table_manual_upload');
    const mediainfo_box = upload_table.querySelector("textarea#release_desc");

    mediainfo_box.addEventListener('input', (event) => {
        handle_autofill(parse_mediainfo(mediainfo_box.value));
    });

    const fasttorrent_box = upload_table.querySelector("input#fasttorrent");
    fasttorrent_box.checked = hasFastTorrent;

    const container_box = upload_table.querySelector("select#format");
    handle_dropdownFill(container_box, defaultFormat);
    
    const source_box = upload_table.querySelector("select#media");
    handle_dropdownFill(source_box, defaultSource);

    const codex_box = upload_table.querySelector("select#bitrate");
    handle_dropdownFill(codex_box, defaultCodex);

    const res_box = upload_table.querySelector("select#resolution");
    handle_dropdownFill(res_box, defaultRes);

    const origin_box = upload_table_alt.querySelector("select#origin");
    handle_dropdownFill(origin_box, defaultOrigin);
})();