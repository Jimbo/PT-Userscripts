// ==UserScript==
// @name         Nyaa Markdown
// @version      1.1.1
// @description  Copy and preview markdown
// @author       Jimbo
// @grant        GM_addStyle
// @grant        GM_setClipboard
// @match        https://nyaa.si/view/*
// @require      https://cdnjs.cloudflare.com/ajax/libs/markdown-it/8.3.1/markdown-it.min.js
// @require      https://unpkg.com/turndown/dist/turndown.js
// @require      https://unpkg.com/turndown-plugin-gfm/dist/turndown-plugin-gfm.js
// @require      https://raw.githubusercontent.com/tengattack/html2bbcode.js/refs/heads/master/html2bbcode.min.js
// ==/UserScript==

(function() {
    'use strict';

    const markdownOptions = {
        html: false,
        breaks: true,
        linkify: true,
        typographer: true
    }
    const markdown = markdownit(markdownOptions);
    markdown.renderer.rules.table_open = function(tokens, idx) {
        return '<table class="table table-striped table-bordered" style="width: auto;">';
    }
    var defaultRender = markdown.renderer.rules.link_open || function(tokens, idx, options, env, self) {
        return self.renderToken(tokens, idx, options);
    }
    markdown.renderer.rules.link_open = function(tokens, idx, options, env, self) {
        tokens[idx].attrPush(['rel', 'noopener nofollow noreferrer']);
        return defaultRender(tokens, idx, options, env, self);
    }

    const gfm = turndownPluginGfm.gfm
    const turndownService = new TurndownService()
    turndownService.use(gfm)

    const submit = document.querySelector('#collapse-comments > form > div:nth-child(2) > div > input')

    if (submit) {
        GM_addStyle('button.btn-preview {margin-left: 5px; color: #fff; background-color: #247fcc; border-color: #247fcc;}')
        GM_addStyle('button.btn-preview:hover {margin-left: 5px; color: #fff; background-color: #19578b; border-color: #19578b;}')
        GM_addStyle('.form-control[hidden] {display: none;}')
        GM_addStyle('.md-preview {height: 8em; overflow: hidden; overflow-y: auto; resize: vertical;}')
    
        submit?.insertAdjacentHTML("afterend", '<button type="button" id="md-preview" class="btn btn-sm btn-preview">Preview</button>')
    
        const button_pre = document.querySelector('button#md-preview')
    
        const comment_input = document.querySelector('textarea#comment')
        comment_input?.insertAdjacentHTML("afterend", '<div class="md-preview form-control"></div>')
        const preview = comment_input?.nextElementSibling;
        preview.hidden = true
        
        button_pre.onclick = () => {
    
            const isCommentHidden = comment_input.hidden;
            comment_input.hidden = !isCommentHidden;
            preview.hidden = isCommentHidden;
    
            console.log(comment_input.value);
            console.log(markdown.render(comment_input.value));
    
            preview.innerHTML = markdown.render(comment_input.value);
        }
    }
    const buttonrow = document.querySelector('body > div > div:nth-child(1) > div.panel-footer.clearfix')

    GM_addStyle('button.btn-copy {margin-right: 5px; color: #fff; background-color: #247fcc; border-color: #247fcc;}')
    GM_addStyle('button.btn-copy:hover {margin-right: 5px; color: #fff; background-color: #19578b; border-color: #19578b;}')
    buttonrow?.insertAdjacentHTML("beforeend", '<button type="button" id="md-copy" class="btn btn-xs btn-copy pull-right">Copy Markdown</button>')
    buttonrow?.insertAdjacentHTML("beforeend", '<button type="button" id="bb-copy" class="btn btn-xs btn-copy pull-right">Copy BBCode</button>')

    const button_cpy = document.querySelector('button#md-copy')
    const md_info = document.querySelector('#torrent-description')

    button_cpy.onclick = () => {
        GM_setClipboard(turndownService.turndown(md_info))
    }


    const button_cpy_bb = document.querySelector('button#bb-copy')
    button_cpy_bb.onclick = () => {
        var converter = new html2bbcode.HTML2BBCode({noheadings: true, nolist: false});
        var bbcode = converter.feed(md_info.innerHTML);
        GM_setClipboard(bbcode.toString())
    }
})();