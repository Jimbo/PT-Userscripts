// ==UserScript==
// @name         Fetch Seadex Best
// @version      1.4
// @description  Gets the best release from Seadex, then shows on the site.
// @grant        GM_xmlhttpRequest
// @downloadURL  https://gitea.com/Jimbo/PT-Userscripts/raw/branch/main/moe-seadex.user.js
// @updateURL    https://gitea.com/Jimbo/PT-Userscripts/raw/branch/main/moe-seadex.user.js
// @match        https://nzbs.moe/series/*
// ==/UserScript==

function gmFetchJson (url, opts = {}, method = 'GET', timeout = 10000) {
	return new Promise((resolve, reject) => {
		GM_xmlhttpRequest({
			method,
			timeout,
			...opts,
			url: url.toString(),
			ontimeout: function () {
				reject(new Error(`Request timed out after ${timeout}ms`))
			},
			onerror: function (err) {
				reject(err || new Error('Failed to fetch'))
			},
			onload: function (response) {
				resolve(JSON.parse(response.responseText))
			}
		})
	})
}

const seadexEndpoint = tinyRest('https://releases.moe/api/collections/entries/records')
// import tinyRest from 'tiny-rest'
// modified for userscripts
function tinyRest (url) {
	const baseURL = new URL(url)

	return (path = './', data = {}) => {
		const requestURL = new URL(path, baseURL)

		for (const [key, value] of Object.entries(data)) requestURL.searchParams.append(key, value)

		requestURL.searchParams.sort() // sort to always have the same order, nicer for caching
		return gmFetchJson(requestURL)
	}
}

function getRelease(filenames, items, release_group) {
	for (const item of items) {
		for (const tr of item.expand.trs) {
			if (!tr.releaseGroup.includes(release_group)) continue
			for (const file of tr.files) {
				const split_name = file.name.split('.')
				if (split_name.at(-1) !== "mkv") continue
				const release_name = split_name.slice(0, -1).join('.')
				if (release_name.search(/NC(ED|OP)/) != -1) continue
				for (const filename of filenames) {
					if (release_name.endsWith(filename) || file.name.endsWith(filename))
						return {item, info: tr}
				}
			}
		}
	}
	return {}
}


function doReleases(items, aniID) {
	const releases = document.querySelectorAll('#releases > tbody')

	for (const release of releases) {

		const release_group = release.innerText.split("/")[0].trim()

		const filenames = [...release.querySelectorAll('tr[data-release-id')].map((tr) => {
			return tr.firstElementChild.innerText.trim()
		})
		const {item, info} = getRelease(filenames, items, release_group)

		if (info) {
			const tr = release.firstElementChild

			release?.setAttribute("isBest", info.isBest)

			const img = document.createElement('img')
			img.src = info.isBest
			  ? 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAJCAYAAABXLP43AAAAAXNSR0IArs4c6QAAAMJJREFUOE9jZBgkgFE1TWwaAwNDJhb3TCdOnHE+K8u/vN9/GLug6qffnvUqC8lcMB+bf5HVgBzyP6skH0PdtJ6JDMSIg9QxMDDOZ2D4nwhSD+EzgD0B49+e9YoR3QKYI2Bq4A6BGoCiHslgnOIINaiOQXMUSD8o1FFCC1kN0SGCy6ewEMUmj2QxONTxqcGaRmCaqB0iWMyFpx+iQwQWN+hph9g0gp4mYKEFM5fiXPP/z//5bBwMROUafDkJIzUPVLECALBqyRj71YzpAAAAAElFTkSuQmCC'
			  : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAJCAYAAABXLP43AAAAAXNSR0IArs4c6QAAALJJREFUOE/NlMEOgkAMRN961Z/kqAfOfoLxA+S4n+lJMd1QUmohTTRRbi2zs9NhSuFPntLBDTgGeoZM/wl1D/0drhN+qHAyvK2O5rUYETKeA9QFyPQFJ2J20AleaqANoXWF4q9QEYqZhUwEC7whXu0rxotxouS8uL5wy2LSjqxNqo5G783FzfUtTJgRPfRtRwLeOT9pR/Tb+OxkM+IzoW4p78db84B6SG7N1ia9pflXv5UXtZlmWNmuM34AAAAASUVORK5CYII='
			img.title = (item.notes ? ` Seadex Notes:\n${item.notes}` : '')
			img.alt = 'Seadex Choice!'
			img.dataset.seadex = ''
			img.onclick = e => {
				e.preventDefault()
				e.stopImmediatePropagation()
				window.open(`https://releases.moe/${aniID}`, '_blank').focus()
			}
			tr.firstElementChild?.insertAdjacentText("beforeend", " / ")
			tr.firstElementChild.append(img)
		}
	}
}

(async function() {
	'use strict';

	const id = document.querySelector('a[href*="anilist.co/"]')?.href.split("/").at(-1)
	if (!id) return

	const { items } = await seadexEndpoint('', { filter: `alID=${id}`, expand: 'trs', fields: '*,expand.trs.url,expand.trs.isBest,expand.trs.files,expand.trs.releaseGroup', skipTotal: true })
	doReleases(items, id)
})();