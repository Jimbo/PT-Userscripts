// ==UserScript==
// @name         BTN comma separated statistics
// @version      1.0
// @description  Fixes numbers that are not comma separated to be comma separated
// @author       Jimbo
// @match        https://broadcasthe.net/user.php?*
// @icon         https://broadcasthe.net/favicon.ico
// @run-at       document-end
// ==/UserScript==

(function() {
    'use strict';
    for (const stat of document.querySelectorAll('#section2 > div > div.statistics li')) {
        if (stat.querySelector('a[href^="user.php"]')) break

        for (const node of stat.childNodes) {
            node.textContent = node.textContent.replaceAll(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
    }
})();
