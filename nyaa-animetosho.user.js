// ==UserScript==
// @name         Nyaa AnimeTosho Extender
// @version      0.5.1
// @description  Adds AnimeTosho NZB link and file info to Nyaa
// @author       Jimbo
// @grant        GM_xmlhttpRequest
// @grant        GM_openInTab
// @match        https://nyaa.si/view/*
// @icon         https://nyaa.si/favicon.ico
// @run-at       document-end
// ==/UserScript==

const SAB_URL = "http://ip:port/"
const NZB_KEY = ""

function fetchAnimeTosho(hash, timeout = 10000) {
    return new Promise((resolve, reject) => {
        GM_xmlhttpRequest({
            headers: {
                "Accept": "application/json"
            },
            method: "GET",
            url: `https://feed.animetosho.org/json?show=torrent&btih=${hash}` ,
            timeout,
            ontimeout: function() {
                reject(new Error(`Request timed out after ${timeout}ms`));
            },
            onerror: function(err) {
                reject(err ? err : new Error('Failed to fetch'))
            },
            onload: function(response) {
                console.log('onload', response)
                resolve(JSON.parse(response.responseText));
            }
        })
    });
}

async function doNZB() {
    const hash = document.querySelector("body > div.container div.panel-body div.col-md-5 > kbd")?.textContent;

    const tosho = await fetchAnimeTosho(hash);
    const magnet = document.querySelector("div > a.card-footer-item");

    const parent = magnet?.parentElement;

    if ("nzb_url" in tosho && tosho.nzb_url) {

        let text = document.createTextNode(" or ")
        parent?.appendChild(text)

        const nzb_sab = magnet?.cloneNode(true);
        
        nzb_sab.querySelector("i").remove()
        nzb_sab.innerHTML = "<img style='margin-right: 2px' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAYAAABWdVznAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAABnSURBVChTY/z//z8DLmBSvQMueabVgxFEM4F5eABMIVwzyAZc2LhqO5BC5RO0ARmAbAP7AdmteAAjUAMDE1AxmEMIf31xC2woyEkgBl4MUwwCJPkBBAaRhutz80CeJ04DTDEDAwMDAGYMS+qci3WZAAAADmVYSWZNTQAqAAAACAAAAAAAAADSU5MAAAAASUVORK5CYII='></img>"
        nzb_sab.href = tosho.nzb_url
        parent?.appendChild(nzb_sab);
        if (NZB_KEY) {
            nzb_sab.innerHTML += "SabNZB"
            nzb_sab.onclick = function() {
                GM_xmlhttpRequest({
                    headers: {
                        "Accept": "application/json"
                    },
                    method: "GET",
                    url: `${SAB_URL}api?mode=addurl&name=${encodeURIComponent(tosho.nzb_url)}&apikey=${NZB_KEY}`,
                    timeout: 5000
                })
                return false
            };
        } else {
            nzb_sab.innerHTML += "NZB"
        }

    }
    if (tosho.files) {
        for (const file of tosho.files) {
            if (file.file_info_type !== "mediainfo") continue
            
            const filename = file.filename.toLowerCase()
            if ((filename.startsWith("extra") || filename.startsWith("bonus") || filename.startsWith("special") || filename.startsWith("creditless")) && filename.includes("/")) continue
            if (!filename.endsWith(".mkv") && !filename.endsWith(".mp4")) continue
            let text = document.createTextNode(" or ")
            parent?.appendChild(text)
    
            const mediainfo = magnet?.cloneNode(true);
    
            mediainfo.querySelector("i").remove()
            mediainfo.innerHTML = "<img style='margin-right: 2px' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAANCAYAAACdKY9CAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAABmSURBVChTY1SpP8Pw7/nZ/wxEACZJY0ZGpbSZ//c3hUCFcIPX7z4whE3Yw8AC4jjWrWGQVNICS+ACyxIg8mAbQAygdWABXECc8zvD83vXGJigfKLBqAZCABTTtLPh5XdORgYGBgYAovkZAiIWQNcAAAAOZVhJZk1NACoAAAAIAAAAAAAAANJTkwAAAABJRU5ErkJggg=='></img>Fileinfo"
            mediainfo.href = "#"
            mediainfo.onclick = function() {
                const fileInfo = file.file_info;
                const htmlContent = `
                    <!DOCTYPE html>
                    <html>
                    <head>
                        <style>
                            body {
                                background-color: #121212;
                                color: #ffffff;
                                font-family: Arial, sans-serif;
                                padding: 0px;
                            }
                            pre {
                                white-space: pre-wrap;
                                word-wrap: break-word;
                            }
                        </style>
                    </head>
                    <body>
                        <pre>${fileInfo}</pre>
                    </body>
                    </html>
                `;
                const blob = new Blob([htmlContent], { type: 'text/html;charset=utf-8' });
                const url = URL.createObjectURL(blob);
                window.open(url, '_blank');
    
                return false
            }
            parent?.appendChild(mediainfo);
            break
        }
    }

    if (tosho.nyaa_id || tosho.anidex_id || tosho.tosho_id) {

        let text = document.createTextNode(" or ")
        parent?.appendChild(text)
        const at = magnet?.cloneNode(true);

        let url = 'https://animetosho.org/view/';

        if (tosho.nyaa_id)
            url += `.n${tosho.nyaa_id}`
        else if (tosho.anidex_id)
            url += `.d${tosho.anidex_id}`
        else if (tosho.tosho_id)
            url += `${tosho.tosho_id}`

    
        at.querySelector("i").remove()
        at.innerHTML = "<img style='margin-right: 2px' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAYAAABWdVznAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAACsSURBVChTfZArDkJBDEX7RmEJ8lkkEoljH4QFsAfCHhBINoJDIpFYJMHihpxm7tBHGI7pZ9rbTjsLTJabPF6sS/TheT7a47T32trQKo7cdvPOG2IxaiDFfnXIr/vVeOfNk9PtJWM1WjEoJ9GEEx9iMWjyqJ+5TUpIBZ9m9pUfScU6sZnd8aXMP2DQEFEBVvszre7MCvFav/Czfl+kBWtim6q6WgkrieQ/5SFmbz7Oa5StxyFtAAAADmVYSWZNTQAqAAAACAAAAAAAAADSU5MAAAAASUVORK5CYII='></img>Animetosho"
        at.href = url
        at.onclick = function(e) {
            e.preventDefault()
            e.stopImmediatePropagation()
            window.open(url, '_blank').focus()
        };
        parent?.appendChild(at);
    }

    if (tosho.primary_file_id) {

        let text = document.createTextNode(" or ")
        parent?.appendChild(text)
        const at = magnet?.cloneNode(true);

        let filename = tosho.torrent_name
        filename = filename.substring(0, filename.lastIndexOf('.')) || filename
    
        at.querySelector("i").remove()
        at.innerHTML = "<img style='margin-right: 2px' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAOCAYAAAAbvf3sAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAB9SURBVChTY1SpP/OfgQC43WDMCGUyMIA0AAEDMoaJodMgzATVBweqDWf/o5gIBCA+SBzExtCALIkMYOIsUD4coNuAoRmfH9AxSJwoPyADDA0ggM0PMIBVAz6AogHZZFy2YA1WkvwAMhmELe9NxK4JVxBiwyC1RCU+BGBgAACRqJh8riNl1AAAAA5lWElmTU0AKgAAAAgAAAAAAAAA0lOTAAAAAElFTkSuQmCC'></img>Attachments"
        at.href = `https://animetosho.org/storage/attachpk/${tosho.primary_file_id}/${filename}_attachments.7z`
        at.onclick = function(e) {
            e.preventDefault()
            e.stopImmediatePropagation()
            window.open(`https://animetosho.org/storage/attachpk/${tosho.primary_file_id}/${filename}_attachments.7z`, '_blank').focus()
        };
        parent?.appendChild(at);
    }
}

(async function() {
    'use strict';
    await doNZB();
})();
